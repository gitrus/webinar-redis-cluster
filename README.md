# webinar-redis-cluster

webinar-redis-cluster

### How to run it?
```bash
cp .env.docker .env
docker-compose build
docker-compose up
```


### Contributors guide:
Install `pipenv`:
https://pypi.org/project/pipenv/

For PyCharm select Pipenv in project interpreter.
`.env.tmp` - the whole list of required env variables
